#!/usr/bin/python

#EG:: ./run_Ondex_performance.py Ondex_performance_WF.xml /Users/joemullen/Desktop/ondex-trunk/trunk/apps/ondex-mini/target/ondex-mini-0.5.0-SNAPSHOT/ scale 1 1

import fileinput
import re
from tempfile import mkstemp
from shutil import move
from os import remove, close
import os
import sys

print "[INFO] Starting performance testing";

input_file = (str(sys.argv[1]))
print 'Original workflow : '+input_file
#the original workflow- this is used to write all other workflows
runMe_location = (str(sys.argv[2]))
print runMe_location
#this is where the runme for Ondex mini is located
searchType = (str(sys.argv[3]))
print 'SearchType : '+searchType
#do we want a linear or a connected graph??
ordersOfMagnitude = (int(sys.argv[4]))
#how many orders of magnitude do we want??
replicates = (int(sys.argv[5]))
#how many times do we want to repeat the experiment?

#OutputFile
output_file = "/Users/joemullen/Dropbox/Ondex_performance/Results/"+searchType+".txt"

workflows_directory = runMe_location + "PERF_workflows/"
os.system("mkdir "+workflows_directory)

lin = "linear"
con = "connected"
scale = "scale"
gSize = "graph size"
gType = "graph type"
opFile = "filename"

#create an individual workflow for one graph of specified size and type
def createWorkflow(size, replicateNumber):
    localFileName = "OP_"+size+"_"+searchType+"_"+str(replicateNumber+1)+".xml"
    fo = open(workflows_directory+localFileName, "wb")
    for line in fileinput.input(input_file):
        line2=line
        if gType in line:
            if searchType == con:
                if lin in line:
                    line2 = line.replace(lin, con);
        if searchType == lin:
                if con in line:
                    line2 = line.replace(con,lin);
        if searchType == scale:
                if con in line:
                    line2 = line.replace(con,scale);
        if gSize in line:
                old =  re.findall(r'\d+', line)
                for i in old:
                    line2 = line.replace(i, size)
        if opFile in line:
                line2 = "<Parameter name=\"filename\">"+ output_file +"</Parameter>"
        fo.write(line2)
    fo.close
    print localFileName

#produce a workflow for every sized graph required
def createAllWorkflows():
    for p in xrange (0,(replicates)):
        for x in xrange (0,(ordersOfMagnitude)):
            for i in xrange(1,10):
                graphsize = (i * (10 **x))
                createWorkflow(str(graphsize),p)

#run all of the workflows
def runAllTheWorkflows():
    files_in_dir = os.listdir(workflows_directory)
    print "cd "+ runMe_location
    for file_i in files_in_dir:
        print file_i
        os.chdir(runMe_location)
        os.system("./runme.sh PERF_workflows/"+file_i)

#call functions
createAllWorkflows()
runAllTheWorkflows()
#delete all the workflows
os.system("rm -r "+workflows_directory)

print "[INFO] Finished Performance Testing";





