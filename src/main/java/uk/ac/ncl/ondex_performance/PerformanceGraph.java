package uk.ac.ncl.ondex_performance;

/*
 * Copyright 2013 Joseph Mullen
 *
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sourceforge.ondex.annotations.*;
import net.sourceforge.ondex.args.ArgumentDefinition;
import net.sourceforge.ondex.args.StringArgumentDefinition;
import net.sourceforge.ondex.core.ConceptClass;
import net.sourceforge.ondex.core.DataSource;
import net.sourceforge.ondex.core.EvidenceType;
import net.sourceforge.ondex.core.ONDEXConcept;
import net.sourceforge.ondex.core.ONDEXRelation;
import net.sourceforge.ondex.core.RelationType;
import net.sourceforge.ondex.parser.ONDEXParser;

/**
 *
 * @author Joseph Mullen
 */
@Authors(authors = {"Joseph Mullen"}, emails = {"j.mullen@ncl.ac.uk"})
@Custodians(custodians = {"Joseph Mullen"}, emails = {"j.mullen@ncl.ac.uk"})
@Status(status = StatusType.STABLE, description = "Performance testing")
public class PerformanceGraph
        extends ONDEXParser {

    String argname0 = "graph size";
    String argdesc0 = "graph size";
    String argname1 = "graph type";
    String argdesc1 = "graph type";
    String argname2 = "filename";
    String argdesc2 = "filename";

    @Override
    public String getId() {
        return PerformanceGraph.class.getSimpleName();
    }

    @Override
    public String getName() {
        return "performance_Graph";
    }

    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public String[] requiresValidators() {
        return new String[0];
    }

    @Override
    public boolean requiresIndexedGraph() {
        return true;
    }

    @Override
    public ArgumentDefinition<?>[] getArgumentDefinitions() {
        return new ArgumentDefinition<?>[]{
            new StringArgumentDefinition(argname0, argdesc0, true, "", false),
            new StringArgumentDefinition(argname1, argdesc1, true, "", false),
            new StringArgumentDefinition(argname2, argdesc2, true, "", false),};
    }

    @Override
    public void start()
            throws Exception {

        String sizeString = (String) getArguments().getUniqueValue(argname0);
        String graphType = (String) getArguments().getUniqueValue(argname1);
        String outPut = (String) getArguments().getUniqueValue(argname2);
        int graphSize = Integer.parseInt(sizeString.trim());
        /*
         * Identify file to append results too
         */
        Writer output;
        output = new BufferedWriter(new FileWriter(outPut, true));

        /*
         * Initialise timers
         */
        long startTime;
        long endTime;

        /*
         * Create some static ConceptClasses, RelationTypes, data sources and evidence types
         */
        graph.getMetaData().createDataSource("PerfDS", "Test data source", "For test purposes");
        graph.getMetaData().createEvidenceType("PerfET", "Test Evidence Type", "For test purposes");
        graph.getMetaData().createRelationType("PerfRT", "Relation full name", "For test purposes",
                "some-relation-inverse", false, false, true, false, null);
        graph.getMetaData().createConceptClass("PerfCC", "per", outPut, null);

        if (graphType.equals("linear")) {
            startTime = System.currentTimeMillis();
            linearGraph(graphSize);
            endTime = System.currentTimeMillis();
            output.append(getGraphBuildTime(endTime, startTime) + "\t" + graphSize + "\t" +getMemUsage()+"\t"+ getGraphSize() + "\n");
            System.out.println(getGraphBuildTime(endTime, startTime) + "\t" + graphSize + "\t" +getMemUsage()+"\t"+ getGraphSize() + "\n");

        } else if (graphType.equals("connected")) {
            startTime = System.currentTimeMillis();
            connectedGraph(graphSize);
            endTime = System.currentTimeMillis();
            output.append(getGraphBuildTime(endTime, startTime) + "\t" + graphSize + "\t" + getMemUsage()+"\t"+getGraphSize() + "\n");
            System.out.println(getGraphBuildTime(endTime, startTime) + "\t" + graphSize + "\t" + getMemUsage()+"\t"+getGraphSize() + "\n");
        } else if (graphType.equals("scale")) {
            startTime = System.currentTimeMillis();
            scaleFreeGraph(graphSize);
            endTime = System.currentTimeMillis();
            output.append(getGraphBuildTime(endTime, startTime) + "\t" + graphSize + "\t" + getMemUsage()+"\t"+getGraphSize() + "\n");
            System.out.println(getGraphBuildTime(endTime, startTime) + "\t" + graphSize + "\t" + getMemUsage()+"\t"+getGraphSize() + "\n");

        } else {
            System.out.println("Graph types may only be: " + "'connected' || 'linear' || 'scale'");
        }

        System.out.println(
                "[JOE INFO] Graph created:" + graph.getConcepts().size() + "/" + graph.getRelations().size());

        output.close();

    }

    public void linearGraph(int size) {

        ConceptClass ccPERF = graph.getMetaData().getConceptClass("PerfCC");
        RelationType rtPERF = graph.getMetaData().getRelationType("PerfRT");
        DataSource dsPERF = graph.getMetaData().getDataSource("PerfDS");
        EvidenceType etPERF = graph.getMetaData().getEvidenceType("PerfET");

        ONDEXConcept previous = null;

        for (int i = 0; i < size; i++) {

            String name = "mtPERF:" + i;
            ONDEXConcept instance = graph.getFactory().createConcept(name, dsPERF, ccPERF, etPERF);

            if (previous != null) {
                ONDEXRelation rel = graph.getFactory().createRelation(previous, instance, rtPERF, etPERF);
            }
            previous = instance;

        }
    }

    public void scaleFreeGraph(int size) {

        ConceptClass ccPERF = graph.getMetaData().getConceptClass("PerfCC");
        RelationType rtPERF = graph.getMetaData().getRelationType("PerfRT");
        DataSource dsPERF = graph.getMetaData().getDataSource("PerfDS");
        EvidenceType etPERF = graph.getMetaData().getEvidenceType("PerfET");

        ONDEXConcept previous = null;

        for (int i = 0; i < size; i++) {
            String name = "myPerf" + i;
            // String ID = "Performance_" + name;
            ONDEXConcept instance = graph.getFactory().createConcept(name, dsPERF, ccPERF, etPERF);
            //add a relation to the previos node
            if (previous != null) {
                ONDEXRelation relprev = graph.getFactory().createRelation(instance, previous, rtPERF, etPERF);
            }
            //add relation to a random node in the graph
            /**
             * NOTE: because these relations are created at random using the
             * SAME semantics (evtype and type) then they may overwrite an edge
             * that already exists- this means that the final stats of the graph
             * may be different to what is expected (n/(n*2)-1), with fewer
             * unique edges created.
             */
            Set<ONDEXConcept> allNodes = graph.getConcepts();
            int nodeSize = allNodes.size();
            int item = new Random().nextInt(nodeSize);
            int n = 0;
            for (ONDEXConcept node : allNodes) {
                if (n == item) {
                    //add relation
                    ONDEXRelation rel = graph.getFactory().createRelation(instance, node, rtPERF, etPERF);
                    break;
                }
                n++;
            }
            previous = instance;
        }
    }

    public void connectedGraph(int size) {

        ConceptClass ccPERF = graph.getMetaData().getConceptClass("PerfCC");
        RelationType rtPERF = graph.getMetaData().getRelationType("PerfRT");
        DataSource dsPERF = graph.getMetaData().getDataSource("PerfDS");
        EvidenceType etPERF = graph.getMetaData().getEvidenceType("PerfET");

        for (int i = 0; i < size; i++) {
            String name = "myPerf" + i;
            // String ID = "Performance_" + name;
            ONDEXConcept instance = graph.getFactory().createConcept(name, dsPERF, ccPERF, etPERF);

            //add relation to every other node in the graph
            Set<ONDEXConcept> allNodes = graph.getConcepts();
            for (ONDEXConcept node : allNodes) {

                if (node != instance) {
                    ONDEXRelation rel = graph.getFactory().createRelation(instance, node, rtPERF, etPERF);
                }
            }
        }
    }

    public String getGraphSize() {
        //get the size of the graph
        Runtime rt = Runtime.getRuntime();
        Process pr = null;

        try {
            pr = rt.exec("du -hs /Users/joemullen/Dropbox/Ondex_performance/test.xml");
        } catch (IOException ex) {
            Logger.getLogger(PerformanceGraph.class.getName()).log(Level.SEVERE, null, ex);
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        String line = null;

        StringBuilder bu = new StringBuilder();

        try {
            while ((line = br.readLine()) != null) {
                bu.append(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(PerformanceGraph.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            pr = rt.exec("rm /Users/joemullen/Dropbox/Ondex_performance/test.xml");
        } catch (IOException ex) {
            Logger.getLogger(PerformanceGraph.class.getName()).log(Level.SEVERE, null, ex);
        }

        return bu.toString().split("\t")[0];
    }

    public String getGraphBuildTime(long en, long st) {
        String time = (" " + (double) (en - st) / 1000);
        return time.trim();
    }

    public long getMemUsage() {
        return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    }
}
